<?php
namespace KooperationsstelleGoettingen\SiteUploadFolder\Hooks;

use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\Exception\SiteNotFoundException;
use TYPO3\CMS\Core\Site\SiteFinder;
use \TYPO3\CMS\Core\Resource\ResourceFactory;

class SiteUploadFolder {
	/**
	 * Get site upload folder
	 *
	 * @param array $params
	 * @param BackendUserAuthentication $beUser
	 * @return Folder
	 */
	public function getSiteUploadFolder($params, BackendUserAuthentication $beUser) {
		$resourceFactory = \TYPO3\CMS\Core\Resource\ResourceFactory::getInstance();

		$site = false;
		$siteFinder = new SiteFinder();
		if ($params['pid'] !== NULL) {
			try {
				$site = $siteFinder->getSiteByPageId($params['pid']);
			} catch (SiteNotFoundException $e) {
				$storage = $resourceFactory->getDefaultStorage();
			}
		} else {
			$storage = $resourceFactory->getDefaultStorage();
		}

		if ($site) {
			$storage = $this->getSiteStorage($site);
		}

		$pagePath = $this->getPagePath($params['pid'], $prepend_path);
		if ($storage->hasFolder($pagePath)) {
			$targetFolder = $storage->getFolder($pagePath);
		} else {
			$targetFolder = $storage->createFolder($pagePath);
		}

		return $targetFolder;
	}

	private function getSiteStorage($site) {
		$resourceFactory = \TYPO3\CMS\Core\Resource\ResourceFactory::getInstance();
		$prepend_path = '';
		try {
			$storage_uid = $site->getAttribute('defaultStorage');
			// if $storage_path is not a number it's a combined path
			// in that case we split the identifier ourselve and
			// prepend the actual path to the generated path later on
			if (!is_numeric($storage_uid)) {
				list($storage_uid, $prepend_path) = explode(":", $storage_uid);
			}
			$storage = $resourceFactory->getStorageObject($storage_uid);
		} catch (\InvalidArgumentException $e) {
			$storage = $resourceFactory->getDefaultStorage();
		}
		return $storage;
	}

	private function getPagePath($pid, $prepend) {
		$rootLine = BackendUtility::BEgetRootLine($pid);
		$pageTitles = array();
		foreach ($rootLine as $page) {
			$pageTitles[] = $this->slugify($page['title']);
		}

		if (empty($pageTitles)) {
			$path = '';
		} else {
			$path = implode('/', $pageTitles) . '/';
		}
		return $prepend . '/' . $path;
	}

	private function slugify($input, $word_delimiter = '_') {
		$slug = iconv('UTF-8', 'ASCII//TRANSLIT', $input);
		$slug = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $slug);
		$slug = strtolower(trim($slug, '-'));
		$slug = preg_replace("/[\/_|+ -]+/", $word_delimiter, $slug);
		return $slug;
	}
}
