# Site upload folder

Allows to use different storage per site for multi tenant sites and saves
uploaded files in an auto generated hierarchy generated from the page titles
in the treeline for the page

## Multi site support

Typo3 9 introduced the site configuration files. We added an option in that file
called `defaultStorage`.

If a file gets uploaded we check if there's a `defaultStorage` configured for
the site the page belongs to and use that storage (you need to configure
it first).

`defaultStorage` can be used in two ways:

1) `defaultStorage = 1`
2) `defaultStorage = 1:/path/to/prepend`

If you use the second option the path in the combined identifier is prepended
to the generated path.

## Menu tree in path

if you have a page that has a couple of parent pages like so
`root page > 1st level > 2nd level > actual page` the file gets uploaded
into a generated folder structure that looks like this
`root_page/1st_level/2nd_level/actual_page/your_file.jpg`. The page titles are
converted to slugs before creating the path structure, so no weird looking URLs!

**Note: There is currently no way to override the generation of the subdirs.**
