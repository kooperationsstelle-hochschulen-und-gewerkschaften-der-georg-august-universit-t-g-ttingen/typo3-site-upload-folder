<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "default_upload_folder"
 ***************************************************************/

$EM_CONF[$_EXTKEY] = [
    'title' => 'Site upload folder',
    'description' => 'Set a default upload folder for the current site in the site configuration file',
    'category' => 'be',
    'state' => 'stable',
    'uploadfolder' => false,
    'createDirs' => '',
    'clearCacheOnLoad' => 1,
    'author' => 'Jan Schrewe',
    'author_email' => 'jan.schrewe@uni-goettingen.de',
    'author_company' => 'Kooperationsstelle Hochschulen und Gewerkschaften der Georg-August-Universität Göttingen',
    'version' => '0.8',
    'constraints' =>
        [
            'depends' =>
                [
                    'typo3' => '9.0.0-9.5.99',
                ],
            'conflicts' =>
                [
                ],
            'suggests' =>
                [
                ],
        ],
    'clearcacheonload' => true,
];
